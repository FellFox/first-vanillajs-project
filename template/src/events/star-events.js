import {EMPTY_STAR} from "../common/constants.js"
import {FULL_STAR} from "../common/constants.js"

let star = JSON.parse(localStorage.getItem('star')) || [];

export const getStar = () => [...star];

export const addStar = (gifId) => {
    if (star.find(id => id === gifId)) {
        return;
    }
    star.shift()
    star.push(gifId);
    localStorage.setItem('star', JSON.stringify(star));
};
export const removeStar = () => {
        star = []
        localStorage.setItem('star', JSON.stringify(star));
      };



export const toggleStarStatus = (gifId) => {
  
      const star = getStar();
      const starSpan = document.querySelector(`span[data-star-id="${gifId}"]`);
      if (star.includes(gifId)) {
        removeStar();
        starSpan.classList.remove('active')
        starSpan.innerHTML = EMPTY_STAR;
      } else {
        addStar(gifId);
        starSpan.classList.add('active');
        starSpan.innerHTML = FULL_STAR;
      }
    };

export const renderStarStatus = (gifId) => {
        const star = getStar();
      
        return star.includes(gifId)
          ? `<span class="star active" data-star-id="${gifId}">${FULL_STAR}</span>`
          : `<span class="star" data-star-id="${gifId}">${EMPTY_STAR}</span>`;
      };
