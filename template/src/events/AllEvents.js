
import { loadHomePage } from "../loads/loadHomePage.js";
import { loadSearchImages } from "../loads/loadSearchResult.js";
import { loadTrendingImages } from "../loads/loadTrendingPage.js";
import { toFavoritesView } from "../views/toFavoritesView.js";
import { toSearchView } from "../views/toSearchView.js";
import { toSingleViewHomePage } from "../views/toSingleViewHomePage.js";
import { toSingleViewTrending } from "../views/toSingleViewTrending.js";
import { getFavorites } from "./favorites-events.js";
import { gifByIdUrl } from '../common/constants.js';
import { getUploaded } from "./uploadGif.js";
import { toSingleViewUploads } from '../views/toSingleViewUploads.js';

export const renderHomePage = () => {
   loadHomePage()
    .then(content => {
        const result = content.data.map(toSingleViewHomePage);
        document.querySelector('.image-container').innerHTML = result.join('');
    })
  };

export const renderSearchPage = () => {
    loadSearchImages()
    .then((content) => {
        const result = content.data.map(toSearchView);
        document.querySelector('#search').value = '';
        document.querySelector('.image-container').innerHTML = result.join('');
   })
};

export const renderTrendingPage = () => {
    loadTrendingImages()
    .then((content) => {
        const result = content.data.map(toSingleViewTrending);
        document.querySelector('.image-container').innerHTML = result.join('');
      })
};

export const renderFavoritesPage = () => {
    document.querySelector('.image-container').innerHTML = '';
    const favorites = getFavorites()
    favorites.map((el) => {
        fetch(`${gifByIdUrl}${el}`)
        .then((response) => response.json())
        .then((content) => {
            content.data.map((el) => {
              document.querySelector('.image-container').innerHTML += toFavoritesView(el);
            });
          })
})
}

export function renderUploadsPage() {
    document.querySelector('.image-container').innerHTML = '';

    const uploaded = getUploaded();
    uploaded.map((el) => {
      fetch(`${gifByIdUrl}${el}`)
        .then((response) => response.json())
        .then((content) => {
          content.data.map((el) => {
            document.querySelector('.image-container').innerHTML += toSingleViewUploads(el);
          });
        })
        .catch((err) => {
          console.error(err);
        });
    });
  }
