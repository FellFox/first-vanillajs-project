let uploaded = JSON.parse(localStorage.getItem('uploaded')) || [];

export const getUploaded = () => [...uploaded];

document.getElementById('read-file').addEventListener('click', (e) => {
  e.preventDefault();

  const userFile = document.getElementById('file').files[0];

  const formData = new FormData();

  if (userFile && userFile !== '') {
    formData.append('file', userFile); 
  } else {
    alert('Invalid file');
  }


  fetch(`https://upload.giphy.com/v1/gifs?api_key=gCEJwZetdOlkZIo1Ol85kbRr0LZXjXA0`, {
    method: 'POST',
    body: formData,
  })
    .then((res) => res.json())
    .then((result) => {
      uploaded.push(result.data.id);
      localStorage.setItem('uploaded', JSON.stringify(uploaded));
    })

    .catch((err) => console.log(err));
});
