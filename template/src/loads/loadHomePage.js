import { trendingOffsetUrl } from '../common/constants.js';

export function loadHomePage() {
  const homePageGifs = fetch(trendingOffsetUrl)
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
    });
  return homePageGifs;
}
