import { searchUrl } from '../common/constants.js';

export function loadSearchImages() {
  let str = document.getElementById('search').value.trim();
  let url = searchUrl.concat(str);

  const searchResult = fetch(url)
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
    });

    return searchResult
}

