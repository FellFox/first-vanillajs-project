import { gifDetailsUrlHead, apiKeyUrlTail } from '../common/constants.js';
import { gifDetailsView } from '../views/gif-details-view.js';

export function gifDetails(id) {
  fetch(`${gifDetailsUrlHead}${id}${apiKeyUrlTail}`)
    .then((response) => response.json())
    .then((details) => {
      document.querySelector('.details').innerHTML = gifDetailsView(details);
    })
    .catch((err) => {
      console.error(err);
    });
}
