# topTensFirstProject

### 1. Description

TopTensFirstProject is a simple web app for viewing, searching, uploading and adding/removing gif images from favorites. It's completely implemented with vanilla JavaScript (Native DOM API) and FontAwesome 6.2.0.

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**

<br>

### 4. Full project view

The finished version of the app has the following views:

- Home
- Trending
- Favorites
- Upload
- (display) Uploads
- Search
- Display details

<br>

#### 4.1 Home

Implementation of home page where limited amount of random gif images are displayed. Each gif image can be added to "favorites" section and/or can be displayed as a "star" gif image.  

#### 4.2 Trending

Implementation of trending page where limited amount of trending gif images are displayed. Each gif image can be added to "favorites" section and/or can be displayed as a "star" gif image. 

<br>

#### 4.3 Favorites
Implementation of "Favorite" gif images are displayed. Each gif image can be removed from "favorites" section and/or can be displayed as a "star" gif image. 

<br>

#### 4.4 Upload

Implementation of the "Upload" functionality allows the users to upload a gif image from their file system. 

<br>

#### 4.5 (display) Uploads

Implementation of the "(display) Uploads" functionality allows users to see their uploaded gif images. 

<br>

#### 4.6 Search

Implementation of the "Search" functionality allows users to search gif images by a given query. 

<br>

#### 4.7 Display details

Implementation of the "Display details" functionality allows users to see a given gif’s detailed information like uploaded user’s username, title, src etc. 

